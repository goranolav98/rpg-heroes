## RPG-Heroes


## Description
This code creates a hero character that can be either mage, warrior, rogue or ranger. Each hero can equip 4 items. One weapon, one head-armor, one body-armor and one leg-armor. Each hero type has their own values for strength, dexterity and intelligence. These values can be increased by either levelling up or equipping items. Each hero has a damage score that can be increased by equipping items or levelling up.  

## Tech Stack
Backend; Java(Gradle project)


## Getting Started
Backend:
* Clone this repository and open it in your IDE(Intellij, Eclipse ect.)
     Create a hero by entering in main Hero hero = new <HeroType>("heroname");
    * level up your hero by entering in main hero.levelUp();
    * add a weapon by entering in main Weapon weapon = new Weapon("WeaponName", requiredlevel,WeaponType, damage);
    * add armor by enterring in main Armor armor = new Armor("ArmorName",requiredLevel,ArmorType, ArmorSlot,   new HeroAttribute(ArmorStrenght,ArmorDexterity,ArmorIntelligence));
    * create a display of the hero by enterring hero.display() in main. 


## Authors and acknowledgment
Gøran Fossen-Helle


