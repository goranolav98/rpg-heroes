package no.experis;

import no.experis.heroes.Hero;
import no.experis.heroes.HeroAttribute;
import no.experis.heroes.exceptions.InvalidWeaponException;
import no.experis.heroes.items.Armor;
import no.experis.heroes.items.Item;
import no.experis.heroes.items.Slot;
import no.experis.heroes.items.Weapon;
import no.experis.heroes.items.itemType.ArmorType;
import no.experis.heroes.types.Mage;
import no.experis.heroes.types.Ranger;
import no.experis.heroes.types.Rogue;
import no.experis.heroes.types.Warrior;

import static no.experis.heroes.items.itemType.WeaponType.STAFF;

public class Main {
    public static void main(String[] args) throws InvalidWeaponException {
        Hero hero = new Mage("Goran");
        Hero hero1= new Ranger("Anna");
        Hero hero2= new Rogue("Jan");
        Hero hero3= new Warrior("Anne");


        //System.out.println(hero.getLevel());
        System.out.println(hero.display());

        hero.levelUp();

        System.out.println(hero.display());

        System.out.println(hero1.display());


        hero1.levelUp();

        System.out.println(hero2.display());

        System.out.println(hero3.display());
        System.out.println(hero.validWeaponTypes);
        System.out.println(hero3.validWeaponTypes);
        System.out.println(hero.validArmorTypes);
        //System.out.println(hero.);


        Weapon weapon = new Weapon("knerten", 1,STAFF, 1);
        //Weapon weapon2 = new Weapon("AXE", 2);
        Armor armor = new Armor("Plata",1,ArmorType.CLOTH, Slot.HEAD,
                new HeroAttribute(2,2,2));

        System.out.println(weapon.getWeaponDamage());
        hero.equipWeapon(weapon);
        //hero.equipArmor();
        System.out.println(hero.display());

        //hero.addWeapon(Item.Slot.WEAPON, weapon);
        //hero.addWeapon(Item.Slot.WEAPON, weapon2);








    }
}