package no.experis.heroes.items;


import no.experis.heroes.HeroAttribute;
import no.experis.heroes.items.itemType.ArmorType;

public class Armor extends Item{

    private final ArmorType armorType;

    private HeroAttribute armorAttribute;

    public Armor(String itemName, int requiredLevel, ArmorType armorType,Slot slot ,HeroAttribute armorAttribute) {
        super(itemName, requiredLevel);
        this.armorType = armorType;
        this.slot = slot;
        this.armorAttribute = armorAttribute;
    }

    public ArmorType getArmorType() {
        return armorType;
    }

    public HeroAttribute getArmorAttribute() {
        return armorAttribute;
    }



}
