package no.experis.heroes.items;


import no.experis.heroes.items.itemType.WeaponType;

public class Weapon extends Item {

    private final WeaponType weaponType;
    public int weaponDamage;
    public Weapon(String itemName, int requiredLevel, WeaponType weaponType, int weaponDamage) {
        super(itemName, requiredLevel );
        this.weaponType = weaponType;
        this.slot = Slot.WEAPON;
        this.weaponDamage = weaponDamage;
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }

    public int getWeaponDamage() {
        return weaponDamage;
    }
}
