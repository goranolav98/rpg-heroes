package no.experis.heroes.items;

import java.util.HashMap;

public class Item {
    public String itemName;
    public int requiredLevel;
    protected Slot slot;

    public Slot getSlot() {
        return slot;
    }

    public Item(String itemName, int requiredLevel) {
        this.itemName = itemName;
        this.requiredLevel = requiredLevel;
    }
    public int getRequiredLevel() {
        return requiredLevel;
    }

    public String getItemName() {
        return itemName;
    }

}
