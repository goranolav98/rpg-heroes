package no.experis.heroes.items;

public enum Slot {
    WEAPON,
    HEAD,
    BODY,
    LEGS
}
