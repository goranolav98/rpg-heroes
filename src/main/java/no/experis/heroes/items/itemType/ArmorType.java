package no.experis.heroes.items.itemType;

public enum ArmorType {
        CLOTH,
        LEATHER,
        MAIL,
        PLATE
}
