package no.experis.heroes.items.itemType;

import no.experis.heroes.items.Weapon;

public enum WeaponType {
    AXE,
    BOW,
    DAGGER,
    HAMMER,
    STAFF,
    SWORD,
    WAND;
}
