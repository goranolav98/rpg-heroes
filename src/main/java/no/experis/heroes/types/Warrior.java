package no.experis.heroes.types;

import no.experis.heroes.Hero;
import no.experis.heroes.HeroAttribute;
import no.experis.heroes.items.itemType.ArmorType;
import no.experis.heroes.items.itemType.WeaponType;

import java.util.ArrayList;
import java.util.Arrays;

public class Warrior extends Hero {



    public Warrior(String name) {
        super(name, new HeroAttribute(5, 2, 1));
        validWeaponTypes = new ArrayList<>(Arrays.asList(WeaponType.AXE,WeaponType.HAMMER, WeaponType.SWORD));
        validArmorTypes = new ArrayList<>(Arrays.asList(ArmorType.PLATE,ArmorType.MAIL));

    }

    /**
     * Increases level by one and adds the correct values to levelattributes.
     */
    @Override
    public void levelUp() {
        super.levelUp();
        levelAttributes.setStrength(levelAttributes.getStrength() + 3);
        levelAttributes.setDexterity(levelAttributes.getDexterity() + 2);
        levelAttributes.setIntelligence(levelAttributes.getIntelligence() + 1);
    }
}
