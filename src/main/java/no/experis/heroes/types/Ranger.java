package no.experis.heroes.types;

import no.experis.heroes.Hero;
import no.experis.heroes.HeroAttribute;
import no.experis.heroes.items.itemType.ArmorType;
import no.experis.heroes.items.itemType.WeaponType;

import java.util.ArrayList;
import java.util.Arrays;

public class Ranger extends Hero {


    public Ranger(String name) {
        super(name, new HeroAttribute(1, 7, 1));
        validWeaponTypes = new ArrayList<>(Arrays.asList(WeaponType.BOW));
        validArmorTypes = new ArrayList<>(Arrays.asList(ArmorType.LEATHER,ArmorType.MAIL));


    }

    /**
     * Increases level by one and adds the correct values to levelattributes.
     */
    @Override
    public void levelUp() {
        super.levelUp();
        levelAttributes.setStrength(levelAttributes.getStrength() + 1);
        levelAttributes.setDexterity(levelAttributes.getDexterity() + 5);
        levelAttributes.setIntelligence(levelAttributes.getIntelligence() + 1);

    }
}

