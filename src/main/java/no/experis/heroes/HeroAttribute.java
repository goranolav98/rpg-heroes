package no.experis.heroes;



public class HeroAttribute {
    protected double strength;
    protected double dexterity;
    protected double intelligence;

    public HeroAttribute(double strength, double dexterity, double intelligence) {

        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }


    public double getStrength() {
        return strength;
    }

    public void setStrength(double strength) {
        this.strength = strength;
    }

    public double getDexterity() {
        return dexterity;
    }

    public void setDexterity(double dexterity) {
        this.dexterity = dexterity;
    }

    public double getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(double intelligence) {
        this.intelligence = intelligence;
    }

    @Override
    public String toString() {
        return "Level Attributes " +
                "strength=" + strength +
                ", dexterity=" + dexterity +
                ", intelligence=" + intelligence;
    }
}

