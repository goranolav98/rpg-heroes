package no.experis.heroes;


import no.experis.heroes.exceptions.InvalidArmorException;
import no.experis.heroes.exceptions.InvalidWeaponException;
import no.experis.heroes.items.Armor;
import no.experis.heroes.items.Item;
import no.experis.heroes.items.Slot;
import no.experis.heroes.items.Weapon;
import no.experis.heroes.items.itemType.ArmorType;
import no.experis.heroes.items.itemType.WeaponType;
import no.experis.heroes.types.Mage;
import no.experis.heroes.types.Ranger;
import no.experis.heroes.types.Rogue;
import no.experis.heroes.types.Warrior;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public abstract class Hero {

    final String name;
    public int level = 1;
    protected HashMap<Slot, Item> equipment = new HashMap<>();

    public HeroAttribute levelAttributes;
    public ArrayList<WeaponType> validWeaponTypes;
    public ArrayList<ArmorType> validArmorTypes ;
    public Hero(String name, HeroAttribute heroAttribute) {
        this.name = name;
        this.levelAttributes = heroAttribute;
        equipment.put(Slot.WEAPON, null);
        equipment.put(Slot.BODY, null);
        equipment.put(Slot.HEAD, null);
        equipment.put(Slot.LEGS, null);

    }

    public void levelUp() {
        level +=1;
    }


    /**
     * checks the Armor it is allowed to be equipped by a Hero.
     * If yes, equip.
     * If no, throw exception.
     * @param armor
     * @throws InvalidArmorException
     */
    public void equipArmor(Armor armor) throws InvalidArmorException {
        if (level >= armor.getRequiredLevel() && validArmorTypes.contains(armor.getArmorType())) {
            equipment.put(armor.getSlot(), armor);
        } else {
            throw new InvalidArmorException("Can not equip armor!");
        }
    }

    /**
     * checks the Weapon it is allowed to be equipped by a Hero.
     * If yes, equip.
     * If no, throw exception.
     * @param weapon
     * @throws InvalidWeaponException
     */
    public void equipWeapon(Weapon weapon) throws InvalidWeaponException {

        if (level >= weapon.getRequiredLevel() && validWeaponTypes.contains(weapon.getWeaponType())) {
            equipment.put(Slot.WEAPON, weapon);
        } else {
            throw new InvalidWeaponException("Can not equip weapon!");
        }
    }

    /**
     * calculates the total attributes of a Hero.
     * @return a list of totalAttribute strength, dexterity, and intelligence
     */
    public List<Float> totalAttributes() {
        List<Float> totalAttributes = new ArrayList<>();
        float totalStrength = (float) levelAttributes.getStrength();
        float totalDexterity = (float) levelAttributes.getDexterity();
        float totalIntelligence = (float) levelAttributes.getIntelligence();
        for (Map.Entry<Slot, Item> value : equipment.entrySet()) {
            if ((value.getKey() != Slot.WEAPON) && (value.getValue() != null)) {
                totalStrength +=
                        ((Armor)value.getValue())
                                .getArmorAttribute()
                                .getStrength();
                totalDexterity +=
                        ((Armor)value.getValue())
                                .getArmorAttribute()
                                .getDexterity();
                totalIntelligence +=
                        ((Armor)value.getValue()).
                                getArmorAttribute().
                                getIntelligence();
            }
        }
        totalAttributes.add(totalStrength);
        totalAttributes.add(totalDexterity);
        totalAttributes.add(totalIntelligence);
        return totalAttributes;
    }

    /**
     * calculates the total damage attribute for each hero.
     * @return a float containing the damage attribute of a Hero.
     */
    public float damage() {
        float totalStrength = totalAttributes().get(0);
        float totalDexterity = totalAttributes().get(1);
        float totalIntelligence = totalAttributes().get(2);
        float damagingAttribute = 0;
        if (equipment.get(Slot.WEAPON) == null){
            damagingAttribute = 1;
        } else if(this.getClass() == Mage.class) {
            damagingAttribute = ((Weapon)equipment.get(Slot.WEAPON)).getWeaponDamage() * (1+ totalIntelligence/100);
        } else if(this.getClass() == Ranger.class) {
            damagingAttribute = ((Weapon)equipment.get(Slot.WEAPON)).getWeaponDamage() * (1+ totalDexterity/100);
        } else if(this.getClass() == Rogue.class) {
            damagingAttribute = ((Weapon)equipment.get(Slot.WEAPON)).getWeaponDamage() * (1+ totalDexterity/100);
        } else if(this.getClass() == Warrior.class) {
            damagingAttribute = ((Weapon)equipment.get(Slot.WEAPON)).getWeaponDamage() * (1+ totalStrength/100);
        }
        return damagingAttribute;
    }

    /**
     * displays details of the current state of a hero.
     * @return a string containing current state of a Hero.
     */
    public String display(){
        return "Name = " + name + '\n' +
                "Class = " + getClass().getSimpleName() + '\n' +
                "Level = " + level  +'\n' +
                "Total Strength = " + totalAttributes().get(0) +'\n' +
                "Total Dexterity = " + totalAttributes().get(1) +'\n' +
                "Total Intelligence = " + totalAttributes().get(2)+'\n' +
                "Total Damage = " + damage()
                ;

    }


    public int getLevel() {
        return level;
    }


    public String getName() {
        return name;
    }
    public HeroAttribute getLevelAttributes() {
        return levelAttributes;
    }





}
