package no.experis.heroes.types;

import static org.junit.jupiter.api.Assertions.*;

import no.experis.heroes.Hero;
import no.experis.heroes.HeroAttribute;
import no.experis.heroes.exceptions.InvalidArmorException;
import no.experis.heroes.exceptions.InvalidWeaponException;
import no.experis.heroes.items.Armor;
import no.experis.heroes.items.Slot;
import no.experis.heroes.items.Weapon;
import no.experis.heroes.items.itemType.ArmorType;
import no.experis.heroes.items.itemType.WeaponType;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;



class RogueTest {
    @Test
    public void Rogue_correctName_shouldReturnName() {
        Hero rogue = new Rogue("Goran");
        String expectedName = "Goran";
        String actualName = rogue.getName();

        assertEquals(expectedName, actualName);
    }


    @Test
    public void rogue_correctLevel_shouldReturnLevel() {
        Hero rogue = new Rogue("Goran");
        int expectedLevel = 1;
        int actualLevel = rogue.getLevel();

        assertEquals(expectedLevel, actualLevel);
    }
    @Test
    public void rogue_correctLevel_LevelUp_shouldReturnNewLevel() {
        Hero rogue = new Rogue("Goran");
        int expectedLevel = 2;
        rogue.levelUp();
        int actualLevel = rogue.getLevel();

        assertEquals(expectedLevel, actualLevel);
    }

    @Test
    public void Rogue_attributes_dexterity_shouldReturnDexterity(){
        Hero rogue = new Rogue("Goran");
        HeroAttribute hero = new HeroAttribute(2,6,1);
        double expected = hero.getDexterity();

        HeroAttribute heroRogue = rogue.getLevelAttributes();
        double actual = heroRogue.getDexterity();

        assertEquals(expected,actual);
    }
    @Test
    public void rogue_attributes_strength_shouldReturnStrength(){
        Hero rogue = new Rogue("Goran");
        HeroAttribute hero = new HeroAttribute(2,6,1);
        double expected = hero.getStrength();

        HeroAttribute heroRogue = rogue.getLevelAttributes();
        double actual = heroRogue.getStrength();

        assertEquals(expected,actual);
    }
    @Test
    public void rogue_attributes_intelligence_shouldReturnIntelligence(){
        Hero rogue = new Rogue("Goran");
        HeroAttribute hero = new HeroAttribute(2,6,1);
        double expected = hero.getIntelligence();

        HeroAttribute heroRogue = rogue.getLevelAttributes();
        double actual = heroRogue.getIntelligence();

        assertEquals(expected,actual);
    }
    @Test
    public void Rogue_attributes_levelUp_dexterity_shouldReturnNewDexterity(){
        Hero rogue = new Rogue("Goran");

        double expected = 10;
        rogue.levelUp();

        HeroAttribute heroRogue = rogue.getLevelAttributes();
        double actual = heroRogue.getDexterity();
        assertEquals(expected,actual);
    }
    @Test
    public void Rogue_attributes_levelUp_strength_shouldReturnNewStrength(){
        Hero rogue = new Rogue("Goran");
        double expected = 3;
        rogue.levelUp();

        HeroAttribute heroRogue = rogue.getLevelAttributes();
        double actual = heroRogue.getStrength();
        assertEquals(expected,actual);
    }
    @Test
    public void Rogue_attributes_levelUp_intelligence_shouldReturnNewIntelligence(){
        Hero rogue = new Rogue("Goran");
        double expected = 2;
        rogue.levelUp();


        HeroAttribute heroRogue = rogue.getLevelAttributes();
        double actual = heroRogue.getIntelligence();
        assertEquals(expected,actual);
    }
    @Test
    public void rogue_equip_valid_weapon_shouldNotTrowException(){
        try{
            Hero rogue = new Rogue("Goran");
            Weapon weapon = new Weapon("Matthew Longstaff",1 ,
                    WeaponType.DAGGER, 2);
            rogue.equipWeapon(weapon);
        } catch (Exception e) {
            fail("Should not have thrown any exception");
        }
    }
    @Test
    public void rogue_equip_invalid_Weapon_shouldReturnExceptionForLevel() {

        assertThrows(InvalidWeaponException.class, () -> {
            Hero rogue = new Rogue("Goran");
            Weapon weapon = new Weapon("Matthew Longstaff", 3,
                    WeaponType.DAGGER, 2);
            rogue.equipWeapon(weapon);
        });
    }
    @Test
    public void rogue_equip_invalid_Weapon_Bow_shouldReturnExceptionForType() {
        assertThrows(InvalidWeaponException.class, () -> {
            Hero rogue = new Rogue("Goran");
            Weapon weapon = new Weapon("Matthew Longstaff", 1,
                    WeaponType.BOW, 2);
            rogue.equipWeapon(weapon);
        });
    }
    @Test
    public void rogue_equip_invalid_Weapon_Hammer_shouldReturnExceptionForType() {
        assertThrows(InvalidWeaponException.class, () -> {
            Hero rogue = new Rogue("Goran");
            Weapon weapon = new Weapon("Matthew Longstaff", 1,
                    WeaponType.HAMMER, 2);
            rogue.equipWeapon(weapon);
        });
    }
    @Test
    public void rogue_equip_invalid_Armor_shouldReturnExceptionForLevel() {
        assertThrows(InvalidArmorException.class, () -> {
            Hero rogue = new Rogue("Goran");
            Armor armor = new Armor("Magic Cloth",3, ArmorType.LEATHER, Slot.BODY,
                    new HeroAttribute(2,2,2));
            rogue.equipArmor(armor);
        });
    }
    @Test
    public void rogue_equip_invalid_Armor_shouldReturnExceptionType() {
        assertThrows(InvalidArmorException.class, () -> {
            Hero rogue = new Rogue("Goran");
            Armor armor = new Armor("Magic Cloth", 1, ArmorType.PLATE, Slot.BODY,
                    new HeroAttribute(2,2,2));
            rogue.equipArmor(armor);
        });

    }
    @Test
    public void totalAttributes_noEquipment_shouldReturnCorrectList() {
        Hero rogue = new Rogue("Goran");
        List<Float> expected = new ArrayList<>();
        expected.add(2.0f);
        expected.add(6.0f);
        expected.add(1.0f);
        List<Float> actual = rogue.totalAttributes();

        assertEquals(expected, actual);
    }
    @Test
    public void totalAttributes_OneArmor_shouldReturnCorrectList() throws InvalidArmorException {
        Hero rogue = new Rogue("Goran");
        Armor armor = new Armor("Magic Cloth", 1, ArmorType.LEATHER,
                Slot.BODY, new HeroAttribute(1,2,3));
        rogue.equipArmor(armor);
        List<Float> expected = new ArrayList<>();
        expected.add(3.0f);
        expected.add(8.0f);
        expected.add(4.0f);
        List<Float> actual = rogue.totalAttributes();

        assertEquals(expected, actual);
    }
    @Test
    public void totalAttributes_Two_Armor_shouldReturnCorrectList() throws InvalidArmorException {
        Hero rogue = new Rogue("Goran");
        Armor armor = new Armor("Magic Cloth", 1, ArmorType.LEATHER,
                Slot.BODY, new HeroAttribute(1,2,3));
        Armor armor2 = new Armor("Bizarre Cloth", 1, ArmorType.LEATHER,
                Slot.HEAD, new HeroAttribute(2,4,1));
        rogue.equipArmor(armor);
        rogue.equipArmor(armor2);
        List<Float> expected = new ArrayList<>();
        expected.add(5.0f);
        expected.add(12.0f);
        expected.add(5.0f);
        List<Float> actual = rogue.totalAttributes();

        assertEquals(expected, actual);
    }
    @Test
    public void totalAttributes_Replace_Armor_shouldReturnCorrectList() throws InvalidArmorException {
        Hero rogue = new Rogue("Goran");
        Armor armor = new Armor("Magic Cloth", 1, ArmorType.LEATHER,
                Slot.BODY, new HeroAttribute(1,2,3));
        Armor armor2 = new Armor("Bizarre Cloth", 1, ArmorType.LEATHER,
                Slot.BODY, new HeroAttribute(2,4,1));
        rogue.equipArmor(armor);
        rogue.equipArmor(armor2);
        List<Float> expected = new ArrayList<>();
        expected.add(4.0f);
        expected.add(10.0f);
        expected.add(2.0f);
        List<Float> actual = rogue.totalAttributes();

        assertEquals(expected, actual);
    }
    @Test
    public void damage_noEquipment_shouldReturnDamageOne() {
        Hero rogue = new Rogue("Goran");
        double expected = 1;
        double actual = rogue.damage();

        assertEquals(expected, actual);
    }
    @Test
    public void damage_with_Weapon_shouldReturnTotalDamage() throws InvalidWeaponException {
        Hero rogue = new Rogue("Goran");
        Weapon weapon = new Weapon("Matthew Longstaff", 1,
                WeaponType.DAGGER, 1);
        rogue.equipWeapon(weapon);
        float expected = 1f * (1f + 6f/100f);
        float actual =  rogue.damage();

        assertEquals(expected, actual);
    }
    @Test
    public void damage_replaceWeapon_shouldReturnTotalDamage() throws InvalidWeaponException {
        Hero rogue = new Rogue("Goran");
        Weapon weapon = new Weapon("Matthew Longstaff", 1,
                WeaponType.DAGGER, 1);
        Weapon weapon2 = new Weapon("Sean Longstaff", 1,
                WeaponType.DAGGER, 3);
        rogue.equipWeapon(weapon);
        rogue.equipWeapon(weapon2);

        float expected = 3F*(1F+ 6f/100F);
        float actual = rogue.damage();

        assertEquals(expected, actual);
    }
    @Test
    public void damage_equipWeaponAndArmor_shouldReturnTotalDamage() throws InvalidWeaponException, InvalidArmorException {
        Hero rogue = new Rogue("Goran");
        Weapon weapon = new Weapon("Matthew Longstaff", 1,
                WeaponType.DAGGER, 1);
        Armor armor = new Armor("Magic Cloth", 1, ArmorType.LEATHER,
                Slot.BODY, new HeroAttribute(1,2,3));
        rogue.equipWeapon(weapon);
        rogue.equipArmor(armor);

        float expected = 1F*(1F+ 8F/100F);
        float actual = rogue.damage();

        assertEquals(expected, actual);
    }
    @Test
    public void rogue_display_shouldReturnHerosState() {
        Hero rogue = new Rogue("Goran");

        String expected = "Name = " + rogue.getName() + "\n" +
                "Class = " + rogue.getClass().getSimpleName() + "\n" +
                "Level = " + rogue.getLevel() + "\n" +
                "Total Strength = "+ rogue.totalAttributes().get(0) + "\n" +
                "Total Dexterity = " + rogue.totalAttributes().get(1) + "\n" +
                "Total Intelligence = " + rogue.totalAttributes().get(2) + "\n" +
                "Total Damage = " + rogue.damage();

        String actual = rogue.display();

        assertEquals(expected, actual);
    }

}
