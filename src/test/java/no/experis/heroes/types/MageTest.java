package no.experis.heroes.types;

import no.experis.heroes.Hero;
import no.experis.heroes.HeroAttribute;
import no.experis.heroes.exceptions.InvalidArmorException;
import no.experis.heroes.exceptions.InvalidWeaponException;
import no.experis.heroes.items.Armor;
import no.experis.heroes.items.Slot;
import no.experis.heroes.items.Weapon;
import no.experis.heroes.items.itemType.ArmorType;
import no.experis.heroes.items.itemType.WeaponType;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MageTest {
    @Test
    public void mage_correctName_shouldReturnName() {
        Hero mage = new Mage("Goran");
        String expectedName = "Goran";
        String actualName = mage.getName();

        assertEquals(expectedName, actualName);
    }


    @Test
    public void mage_correctLevel_shouldReturnLevel() {
        Hero mage = new Mage("Goran");
        int expectedLevel = 1;
        int actualLevel = mage.getLevel();

        assertEquals(expectedLevel, actualLevel);
    }
    @Test
    public void mage_correctLevel_LevelUp_shouldReturnNewLevel() {
        Hero mage = new Mage("Goran");
        int expectedLevel = 2;
        mage.levelUp();
        int actualLevel = mage.getLevel();

        assertEquals(expectedLevel, actualLevel);
    }

    @Test
    public void mage_attributes_dexterity_shouldReturnDexterity(){
        Hero mage = new Mage("Goran");
        HeroAttribute hero = new HeroAttribute(1,1,8);
        double expected = hero.getDexterity();

        HeroAttribute heroMage = mage.getLevelAttributes();
        double actual = heroMage.getDexterity();

        assertEquals(expected,actual);
    }
    @Test
    public void mage_attributes_strength_shouldReturnStrength(){
        Hero mage = new Mage("Goran");
        HeroAttribute hero = new HeroAttribute(1,1,8);
        double expected = hero.getStrength();

        HeroAttribute heroMage = mage.getLevelAttributes();
        double actual = heroMage.getStrength();

        assertEquals(expected,actual);
    }
    @Test
    public void mage_attributes_intelligence_shouldReturnIntelligence(){
        Hero mage = new Mage("Goran");
        HeroAttribute hero = new HeroAttribute(1,1,8);
        double expected = hero.getIntelligence();

        HeroAttribute heroMage = mage.getLevelAttributes();
        double actual = heroMage.getIntelligence();

        assertEquals(expected,actual);
    }
    @Test
    public void mage_attributes_levelUp_dexterity_shouldReturnNewDexterity(){
        Hero mage = new Mage("Goran");

        double expected = 2;
        mage.levelUp();

        HeroAttribute heroMage = mage.getLevelAttributes();
        double actual = heroMage.getDexterity();
        assertEquals(expected,actual);
    }
    @Test
    public void mage_attributes_levelUp_strength_shouldReturnNewStrength(){
        Hero mage = new Mage("Goran");
        double expected = 2;
        mage.levelUp();

        HeroAttribute heroMage = mage.getLevelAttributes();
        double actual = heroMage.getStrength();
        assertEquals(expected,actual);
    }
    @Test
    public void mage_attributes_levelUp_intelligence_shouldReturnNewIntelligence(){
        Hero mage = new Mage("Goran");
        double expected = 13;
        mage.levelUp();


        HeroAttribute heroMage = mage.getLevelAttributes();
        double actual = heroMage.getIntelligence();
        assertEquals(expected,actual);
    }
    @Test
    public void mage_equip_valid_weapon_shouldNotTrowException(){
        try{
            Hero mage = new Mage("Goran");
            Weapon weapon = new Weapon("Matthew Longstaff",1 ,
                    WeaponType.STAFF, 2);
            mage.equipWeapon(weapon);
        } catch (Exception e) {
            fail("Should not have thrown any exception");
        }
    }
    @Test
    public void mage_equip_invalid_Weapon_shouldReturnExceptionForLevel() {

        assertThrows(InvalidWeaponException.class, () -> {
            Hero mage = new Mage("Goran");
            Weapon weapon = new Weapon("Matthew Longstaff", 3,
                    WeaponType.STAFF, 2);
            mage.equipWeapon(weapon);
        });
    }
    @Test
    public void mage_equip_invalid_Weapon_Dagger_shouldReturnExceptionForType() {
        assertThrows(InvalidWeaponException.class, () -> {
            Hero mage = new Mage("Goran");
            Weapon weapon = new Weapon("Matthew Longstaff", 1,
                    WeaponType.DAGGER, 2);
            mage.equipWeapon(weapon);
        });
    }
    @Test
    public void mage_equip_invalid_Weapon_Hammer_shouldReturnExceptionForType() {
        assertThrows(InvalidWeaponException.class, () -> {
            Hero mage = new Mage("Goran");
            Weapon weapon = new Weapon("Matthew Longstaff", 1,
                    WeaponType.HAMMER, 2);
            mage.equipWeapon(weapon);
        });
    }
    @Test
    public void mage_equip_invalid_Armor_shouldReturnExceptionForLevel() {
        assertThrows(InvalidArmorException.class, () -> {
            Hero mage = new Mage("Goran");
            Armor armor = new Armor("Magic Cloth",3, ArmorType.CLOTH, Slot.BODY,
                    new HeroAttribute(2,2,2));
            mage.equipArmor(armor);
        });
    }
    @Test
    public void mage_equip_invalid_Armor_shouldReturnExceptionType() {
        assertThrows(InvalidArmorException.class, () -> {
            Hero mage = new Mage("Goran");
            Armor armor = new Armor("Magic Cloth", 1, ArmorType.MAIL, Slot.BODY,
                    new HeroAttribute(2,2,2));
            mage.equipArmor(armor);
        });

    }
    @Test
    public void totalAttributes_noEquipment_shouldReturnCorrectList() {
        Hero mage = new Mage("Goran");
        List<Float> expected = new ArrayList<>();
        expected.add(1.0f);
        expected.add(1.0f);
        expected.add(8.0f);
        List<Float> actual = mage.totalAttributes();

        assertEquals(expected, actual);
    }
    @Test
    public void totalAttributes_OneArmor_shouldReturnCorrectList() throws InvalidArmorException {
        Hero mage = new Mage("Goran");
        Armor armor = new Armor("Magic Cloth", 1, ArmorType.CLOTH,
                Slot.BODY, new HeroAttribute(1,2,3));
        mage.equipArmor(armor);
        List<Float> expected = new ArrayList<>();
        expected.add(2.0f);
        expected.add(3.0f);
        expected.add(11.0f);
        List<Float> actual = mage.totalAttributes();

        assertEquals(expected, actual);
    }
    @Test
    public void totalAttributes_Two_Armor_shouldReturnCorrectList() throws InvalidArmorException {
        Hero mage = new Mage("Goran");
        Armor armor = new Armor("Magic Cloth", 1, ArmorType.CLOTH,
                Slot.BODY, new HeroAttribute(1,2,3));
        Armor armor2 = new Armor("Bizarre Cloth", 1, ArmorType.CLOTH,
                Slot.HEAD, new HeroAttribute(2,4,1));
        mage.equipArmor(armor);
        mage.equipArmor(armor2);
        List<Float> expected = new ArrayList<>();
        expected.add(4.0f);
        expected.add(7.0f);
        expected.add(12.0f);
        List<Float> actual = mage.totalAttributes();

        assertEquals(expected, actual);
    }
    @Test
    public void totalAttributes_Replace_Armor_shouldReturnCorrectList() throws InvalidArmorException {
        Hero mage = new Mage("Goran");
        Armor armor = new Armor("Magic Cloth", 1, ArmorType.CLOTH,
                Slot.BODY, new HeroAttribute(1,2,3));
        Armor armor2 = new Armor("Bizarre Cloth", 1, ArmorType.CLOTH,
                Slot.BODY, new HeroAttribute(2,4,1));
        mage.equipArmor(armor);
        mage.equipArmor(armor2);
        List<Float> expected = new ArrayList<>();
        expected.add(3.0f);
        expected.add(5.0f);
        expected.add(9.0f);
        List<Float> actual = mage.totalAttributes();

        assertEquals(expected, actual);
    }
    @Test
    public void damage_noEquipment_shouldReturnDamageOne() {
        Hero mage = new Mage("Goran");
        double expected = 1;
        double actual = mage.damage();

        assertEquals(expected, actual);
    }
    @Test
    public void damage_with_Weapon_shouldReturnTotalDamage() throws InvalidWeaponException {
        Hero mage = new Mage("Goran");
        Weapon weapon = new Weapon("Matthew Longstaff", 1,
                WeaponType.STAFF, 1);
        mage.equipWeapon(weapon);
        float expected = 1f * (1f + 8f/100f);
        float actual =  mage.damage();

        assertEquals(expected, actual);
    }
    @Test
    public void damage_replaceWeapon_shouldReturnTotalDamage() throws InvalidWeaponException {
        Hero mage = new Mage("Goran");
        Weapon weapon = new Weapon("Matthew Longstaff", 1,
                WeaponType.STAFF, 1);
        Weapon weapon2 = new Weapon("Sean Longstaff", 1,
                WeaponType.STAFF, 3);
        mage.equipWeapon(weapon);
        mage.equipWeapon(weapon2);

        float expected = 3F*(1F+ 8F/100F);
        float actual = mage.damage();

        assertEquals(expected, actual);
    }
    @Test
    public void damage_equipWeaponAndArmor_shouldReturnTotalDamage() throws InvalidWeaponException, InvalidArmorException {
        Hero mage = new Mage("Goran");
        Weapon weapon = new Weapon("Matthew Longstaff", 1,
                WeaponType.STAFF, 1);
        Armor armor = new Armor("Magic Cloth", 1, ArmorType.CLOTH,
                Slot.BODY, new HeroAttribute(1,2,3));
        mage.equipWeapon(weapon);
        mage.equipArmor(armor);

        float expected = 1F*(1F+ 11F/100F);
        float actual = mage.damage();

        assertEquals(expected, actual);
    }
    @Test
    public void mage_display_shouldReturnHerosState() {
        Hero mage = new Mage("goran");

        String expected = "Name = " + mage.getName() + "\n" +
                "Class = " + mage.getClass().getSimpleName() + "\n" +
                "Level = " + mage.getLevel() + "\n" +
                "Total Strength = "+ mage.totalAttributes().get(0) + "\n" +
                "Total Dexterity = " + mage.totalAttributes().get(1) + "\n" +
                "Total Intelligence = " + mage.totalAttributes().get(2) + "\n" +
                "Total Damage = " + mage.damage();

        String actual = mage.display();

        assertEquals(expected, actual);
    }

}
