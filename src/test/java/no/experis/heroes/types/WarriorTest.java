package no.experis.heroes.types;

import static org.junit.jupiter.api.Assertions.*;

import no.experis.heroes.Hero;
import no.experis.heroes.HeroAttribute;
import no.experis.heroes.exceptions.InvalidArmorException;
import no.experis.heroes.exceptions.InvalidWeaponException;
import no.experis.heroes.items.Armor;
import no.experis.heroes.items.Slot;
import no.experis.heroes.items.Weapon;
import no.experis.heroes.items.itemType.ArmorType;
import no.experis.heroes.items.itemType.WeaponType;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class WarriorTest {
    @Test
    public void warrior_correctName_shouldReturnName() {
        Hero warrior = new Warrior("Goran");
        String expectedName = "Goran";
        String actualName = warrior.getName();

        assertEquals(expectedName, actualName);
    }


    @Test
    public void warrior_correctLevel_shouldReturnLevel() {
        Hero warrior = new Warrior("Goran");
        int expectedLevel = 1;
        int actualLevel = warrior.getLevel();

        assertEquals(expectedLevel, actualLevel);
    }
    @Test
    public void warrior_correctLevel_LevelUp_shouldReturnNewLevel() {
        Hero warrior = new Warrior("Goran");
        int expectedLevel = 2;
        warrior.levelUp();
        int actualLevel = warrior.getLevel();

        assertEquals(expectedLevel, actualLevel);
    }

    @Test
    public void Warrior_attributes_dexterity_shouldReturnDexterity(){
        Hero warrior = new Warrior("Goran");
        HeroAttribute hero = new HeroAttribute(5,2,1);
        double expected = hero.getDexterity();

        HeroAttribute heroWarrior = warrior.getLevelAttributes();
        double actual = heroWarrior.getDexterity();

        assertEquals(expected,actual);
    }
    @Test
    public void Warrior_attributes_strength_shouldReturnStrength(){
        Hero warrior = new Warrior("Goran");
        HeroAttribute hero = new HeroAttribute(5,2,1);
        double expected = hero.getStrength();

        HeroAttribute heroWarrior = warrior.getLevelAttributes();
        double actual = heroWarrior.getStrength();

        assertEquals(expected,actual);
    }
    @Test
    public void Warrior_attributes_intelligence_shouldReturnIntelligence(){
        Hero warrior = new Warrior("Goran");
        HeroAttribute hero = new HeroAttribute(5,2,1);
        double expected = hero.getIntelligence();

        HeroAttribute heroWarrior = warrior.getLevelAttributes();
        double actual = heroWarrior.getIntelligence();

        assertEquals(expected,actual);
    }
    @Test
    public void Warrior_attributes_levelUp_dexterity_shouldReturnNewDexterity(){
        Hero warrior = new Warrior("Goran");

        double expected = 4;
        warrior.levelUp();

        HeroAttribute heroWarrior = warrior.getLevelAttributes();
        double actual = heroWarrior.getDexterity();
        assertEquals(expected,actual);
    }
    @Test
    public void Warrior_attributes_levelUp_strength_shouldReturnNewStrength(){
        Hero warrior = new Warrior("Goran");
        double expected = 8;
        warrior.levelUp();

        HeroAttribute heroMage = warrior.getLevelAttributes();
        double actual = heroMage.getStrength();
        assertEquals(expected,actual);
    }
    @Test
    public void Warrior_attributes_levelUp_intelligence_shouldReturnNewIntelligence(){
        Hero warrior = new Warrior("Goran");
        double expected = 2;
        warrior.levelUp();


        HeroAttribute heroWarrior = warrior.getLevelAttributes();
        double actual = heroWarrior.getIntelligence();
        assertEquals(expected,actual);
    }
    @Test
    public void Warrior_equip_valid_weapon_shouldNotTrowException(){
        try{
            Hero warrior = new Warrior("Goran");
            Weapon weapon = new Weapon("Matthew Longstaff",1 ,
                    WeaponType.HAMMER, 2);
            warrior.equipWeapon(weapon);
        } catch (Exception e) {
            fail("Should not have thrown any exception");
        }
    }
    @Test
    public void Warrior_equip_invalid_Weapon_shouldReturnExceptionForLevel() {

        assertThrows(InvalidWeaponException.class, () -> {
            Hero warrior = new Warrior("Goran");
            Weapon weapon = new Weapon("Matthew Longstaff", 3,
                    WeaponType.HAMMER, 2);
            warrior.equipWeapon(weapon);
        });
    }
    @Test
    public void Warrior_equip_invalid_Weapon_Dagger_shouldReturnExceptionForType() {
        assertThrows(InvalidWeaponException.class, () -> {
            Hero warrior = new Warrior("Goran");
            Weapon weapon = new Weapon("Matthew Longstaff", 1,
                    WeaponType.DAGGER, 2);
            warrior.equipWeapon(weapon);
        });
    }
    @Test
    public void Warrior_equip_invalid_Weapon_Bow_shouldReturnExceptionForType() {
        assertThrows(InvalidWeaponException.class, () -> {
            Hero warrior = new Warrior("Goran");
            Weapon weapon = new Weapon("Matthew Longstaff", 1,
                    WeaponType.BOW, 2);
            warrior.equipWeapon(weapon);
        });
    }
    @Test
    public void Warrior_equip_invalid_Armor_shouldReturnExceptionForLevel() {
        assertThrows(InvalidArmorException.class, () -> {
            Hero warrior = new Warrior("Goran");
            Armor armor = new Armor("Magic Cloth",3, ArmorType.PLATE, Slot.BODY,
                    new HeroAttribute(2,2,2));
            warrior.equipArmor(armor);
        });
    }
    @Test
    public void Warrior_equip_invalid_Armor_shouldReturnExceptionType() {
        assertThrows(InvalidArmorException.class, () -> {
            Hero warrior = new Warrior("Goran");
            Armor armor = new Armor("Magic Cloth", 1, ArmorType.CLOTH, Slot.BODY,
                    new HeroAttribute(2,2,2));
            warrior.equipArmor(armor);
        });

    }
    @Test
    public void totalAttributes_noEquipment_shouldReturnCorrectList() {
        Hero warrior = new Warrior("Goran");
        List<Float> expected = new ArrayList<>();
        expected.add(5.0f);
        expected.add(2.0f);
        expected.add(1.0f);
        List<Float> actual = warrior.totalAttributes();

        assertEquals(expected, actual);
    }
    @Test
    public void totalAttributes_OneArmor_shouldReturnCorrectList() throws InvalidArmorException {
        Hero warrior = new Warrior("Goran");
        Armor armor = new Armor("Magic Cloth", 1, ArmorType.PLATE,
                Slot.BODY, new HeroAttribute(1,2,3));
        warrior.equipArmor(armor);
        List<Float> expected = new ArrayList<>();
        expected.add(6.0f);
        expected.add(4.0f);
        expected.add(4.0f);
        List<Float> actual = warrior.totalAttributes();

        assertEquals(expected, actual);
    }
    @Test
    public void totalAttributes_Two_Armor_shouldReturnCorrectList() throws InvalidArmorException {
        Hero warrior = new Warrior("Goran");
        Armor armor = new Armor("Magic Cloth", 1, ArmorType.PLATE,
                Slot.BODY, new HeroAttribute(1,2,3));
        Armor armor2 = new Armor("Bizarre Cloth", 1, ArmorType.PLATE,
                Slot.HEAD, new HeroAttribute(2,4,1));
        warrior.equipArmor(armor);
        warrior.equipArmor(armor2);
        List<Float> expected = new ArrayList<>();
        expected.add(8.0f);
        expected.add(8.0f);
        expected.add(5.0f);
        List<Float> actual = warrior.totalAttributes();

        assertEquals(expected, actual);
    }
    @Test
    public void totalAttributes_Replace_Armor_shouldReturnCorrectList() throws InvalidArmorException {
        Hero warrior = new Warrior("Goran");
        Armor armor = new Armor("Magic Cloth", 1, ArmorType.PLATE,
                Slot.BODY, new HeroAttribute(1,2,3));
        Armor armor2 = new Armor("Bizarre Cloth", 1, ArmorType.PLATE,
                Slot.BODY, new HeroAttribute(2,4,1));
        warrior.equipArmor(armor);
        warrior.equipArmor(armor2);
        List<Float> expected = new ArrayList<>();
        expected.add(7.0f);
        expected.add(6.0f);
        expected.add(2.0f);
        List<Float> actual = warrior.totalAttributes();

        assertEquals(expected, actual);
    }
    @Test
    public void damage_noEquipment_shouldReturnDamageOne() {
        Hero warrior = new Warrior("Goran");
        double expected = 1;
        double actual = warrior.damage();

        assertEquals(expected, actual);
    }
    @Test
    public void damage_with_Weapon_shouldReturnTotalDamage() throws InvalidWeaponException {
        Hero warrior = new Warrior("Goran");
        Weapon weapon = new Weapon("Matthew Longstaff", 1,
                WeaponType.SWORD, 1);
        warrior.equipWeapon(weapon);
        float expected = 1f * (1f + 5f/100f);
        float actual =  warrior.damage();

        assertEquals(expected, actual);
    }
    @Test
    public void damage_replaceWeapon_shouldReturnTotalDamage() throws InvalidWeaponException {
        Hero warrior = new Warrior("Goran");
        Weapon weapon = new Weapon("Matthew Longstaff", 1,
                WeaponType.SWORD, 1);
        Weapon weapon2 = new Weapon("Sean Longstaff", 1,
                WeaponType.SWORD, 3);
        warrior.equipWeapon(weapon);
        warrior.equipWeapon(weapon2);

        float expected = 3F*(1F+ 5F/100F);
        float actual = warrior.damage();

        assertEquals(expected, actual);
    }
    @Test
    public void damage_equipWeaponAndArmor_shouldReturnTotalDamage() throws InvalidWeaponException, InvalidArmorException {
        Hero warrior = new Warrior("Goran");
        Weapon weapon = new Weapon("Matthew Longstaff", 1,
                WeaponType.SWORD, 1);
        Armor armor = new Armor("Magic Cloth", 1, ArmorType.PLATE,
                Slot.BODY, new HeroAttribute(1,2,3));
        warrior.equipWeapon(weapon);
        warrior.equipArmor(armor);

        float expected = 1F*(1F+ 6F/100F);
        float actual = warrior.damage();

        assertEquals(expected, actual);
    }
    @Test
    public void warrior_display_shouldReturnHerosState() {
        Hero warrior = new Warrior("goran");

        String expected = "Name = " + warrior.getName() + "\n" +
                "Class = " + warrior.getClass().getSimpleName() + "\n" +
                "Level = " + warrior.getLevel() + "\n" +
                "Total Strength = "+ warrior.totalAttributes().get(0) + "\n" +
                "Total Dexterity = " + warrior.totalAttributes().get(1) + "\n" +
                "Total Intelligence = " + warrior.totalAttributes().get(2) + "\n" +
                "Total Damage = " + warrior.damage();

        String actual = warrior.display();

        assertEquals(expected, actual);
    }

}
