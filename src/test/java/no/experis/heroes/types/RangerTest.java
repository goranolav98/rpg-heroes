package no.experis.heroes.types;

import static org.junit.jupiter.api.Assertions.*;

import no.experis.heroes.Hero;
import no.experis.heroes.HeroAttribute;
import no.experis.heroes.exceptions.InvalidArmorException;
import no.experis.heroes.exceptions.InvalidWeaponException;
import no.experis.heroes.items.Armor;
import no.experis.heroes.items.Slot;
import no.experis.heroes.items.Weapon;
import no.experis.heroes.items.itemType.ArmorType;
import no.experis.heroes.items.itemType.WeaponType;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;


class RangerTest {
    @Test
    public void Ranger_correctName_shouldReturnName() {
        Hero ranger = new Ranger("Goran");
        String expectedName = "Goran";
        String actualName = ranger.getName();

        assertEquals(expectedName, actualName);
    }


    @Test
    public void ranger_correctLevel_shouldReturnLevel() {
        Hero ranger = new Ranger("Goran");
        int expectedLevel = 1;
        int actualLevel = ranger.getLevel();

        assertEquals(expectedLevel, actualLevel);
    }
    @Test
    public void Ranger_correctLevel_LevelUp_shouldReturnNewLevel() {
        Hero ranger = new Ranger("Goran");
        int expectedLevel = 2;
        ranger.levelUp();
        int actualLevel = ranger.getLevel();

        assertEquals(expectedLevel, actualLevel);
    }

    @Test
    public void Ranger_attributes_dexterity_shouldReturnDexterity(){
        Hero ranger = new Ranger("Goran");
        HeroAttribute hero = new HeroAttribute(1,7,1);
        double expected = hero.getDexterity();

        HeroAttribute heroRanger = ranger.getLevelAttributes();
        double actual = heroRanger.getDexterity();

        assertEquals(expected,actual);
    }
    @Test
    public void ranger_attributes_strength_shouldReturnStrength(){
        Hero ranger = new Ranger("Goran");
        HeroAttribute hero = new HeroAttribute(1,7,1);
        double expected = hero.getStrength();

        HeroAttribute heroRanger = ranger.getLevelAttributes();
        double actual = heroRanger.getStrength();

        assertEquals(expected,actual);
    }
    @Test
    public void Ranger_attributes_intelligence_shouldReturnIntelligence(){
        Hero ranger = new Ranger("Goran");
        HeroAttribute hero = new HeroAttribute(1,7,1);
        double expected = hero.getIntelligence();

        HeroAttribute heroRanger = ranger.getLevelAttributes();
        double actual = heroRanger.getIntelligence();

        assertEquals(expected,actual);
    }
    @Test
    public void Ranger_attributes_levelUp_dexterity_shouldReturnNewDexterity(){
        Hero ranger = new Ranger("Goran");

        double expected = 12;
        ranger.levelUp();

        HeroAttribute heroRanger = ranger.getLevelAttributes();
        double actual = heroRanger.getDexterity();
        assertEquals(expected,actual);
    }
    @Test
    public void Ranger_attributes_levelUp_strength_shouldReturnNewStrength(){
        Hero ranger = new Ranger("Goran");
        double expected = 2;
        ranger.levelUp();

        HeroAttribute heroRanger = ranger.getLevelAttributes();
        double actual = heroRanger.getStrength();
        assertEquals(expected,actual);
    }
    @Test
    public void Ranger_attributes_levelUp_intelligence_shouldReturnNewIntelligence(){
        Hero ranger = new Ranger("Goran");
        double expected = 2;
        ranger.levelUp();


        HeroAttribute heroRanger = ranger.getLevelAttributes();
        double actual = heroRanger.getIntelligence();
        assertEquals(expected,actual);
    }
    @Test
    public void ranger_equip_valid_weapon_shouldNotTrowException(){
        try{
            Hero ranger = new Ranger("Goran");
            Weapon weapon = new Weapon("Matthew Longstaff",1 ,
                    WeaponType.BOW, 2);
            ranger.equipWeapon(weapon);
        } catch (Exception e) {
            fail("Should not have thrown any exception");
        }
    }
    @Test
    public void ranger_equip_invalid_Weapon_shouldReturnExceptionForLevel() {

        assertThrows(InvalidWeaponException.class, () -> {
            Hero ranger = new Ranger("Goran");
            Weapon weapon = new Weapon("Matthew Longstaff", 3,
                    WeaponType.BOW, 2);
            ranger.equipWeapon(weapon);
        });
    }
    @Test
    public void ranger_equip_invalid_Weapon_Dagger_shouldReturnExceptionForType() {
        assertThrows(InvalidWeaponException.class, () -> {
            Hero ranger = new Ranger("Goran");
            Weapon weapon = new Weapon("Matthew Longstaff", 1,
                    WeaponType.DAGGER, 2);
            ranger.equipWeapon(weapon);
        });
    }
    @Test
    public void ranger_equip_invalid_Weapon_Hammer_shouldReturnExceptionForType() {
        assertThrows(InvalidWeaponException.class, () -> {
            Hero ranger = new Ranger("Goran");
            Weapon weapon = new Weapon("Matthew Longstaff", 1,
                    WeaponType.HAMMER, 2);
            ranger.equipWeapon(weapon);
        });
    }
    @Test
    public void ranger_equip_invalid_Armor_shouldReturnExceptionForLevel() {
        assertThrows(InvalidArmorException.class, () -> {
            Hero ranger = new Ranger("Goran");
            Armor armor = new Armor("Magic Cloth",3, ArmorType.LEATHER, Slot.BODY,
                    new HeroAttribute(2,2,2));
            ranger.equipArmor(armor);
        });
    }
    @Test
    public void Ranger_equip_invalid_Armor_shouldReturnExceptionType() {
        assertThrows(InvalidArmorException.class, () -> {
            Hero ranger = new Ranger("Goran");
            Armor armor = new Armor("Magic Cloth", 1, ArmorType.PLATE, Slot.BODY,
                    new HeroAttribute(2,2,2));
            ranger.equipArmor(armor);
        });

    }
    @Test
    public void totalAttributes_noEquipment_shouldReturnCorrectList() {
        Hero ranger = new Ranger("Goran");
        List<Float> expected = new ArrayList<>();
        expected.add(1.0f);
        expected.add(7.0f);
        expected.add(1.0f);
        List<Float> actual = ranger.totalAttributes();

        assertEquals(expected, actual);
    }
    @Test
    public void totalAttributes_OneArmor_shouldReturnCorrectList() throws InvalidArmorException {
        Hero ranger = new Ranger("Goran");
        Armor armor = new Armor("Magic Cloth", 1, ArmorType.LEATHER,
                Slot.BODY, new HeroAttribute(1,2,3));
        ranger.equipArmor(armor);
        List<Float> expected = new ArrayList<>();
        expected.add(2.0f);
        expected.add(9.0f);
        expected.add(4.0f);
        List<Float> actual = ranger.totalAttributes();

        assertEquals(expected, actual);
    }
    @Test
    public void totalAttributes_Two_Armor_shouldReturnCorrectList() throws InvalidArmorException {
        Hero ranger = new Ranger("Goran");
        Armor armor = new Armor("Magic Cloth", 1, ArmorType.LEATHER,
                Slot.BODY, new HeroAttribute(1,2,3));
        Armor armor2 = new Armor("Bizarre Cloth", 1, ArmorType.LEATHER,
                Slot.HEAD, new HeroAttribute(2,4,1));
        ranger.equipArmor(armor);
        ranger.equipArmor(armor2);
        List<Float> expected = new ArrayList<>();
        expected.add(4.0f);
        expected.add(13.0f);
        expected.add(5.0f);
        List<Float> actual = ranger.totalAttributes();

        assertEquals(expected, actual);
    }
    @Test
    public void totalAttributes_Replace_Armor_shouldReturnCorrectList() throws InvalidArmorException {
        Hero ranger = new Ranger("Goran");
        Armor armor = new Armor("Magic Cloth", 1, ArmorType.LEATHER,
                Slot.BODY, new HeroAttribute(1,2,3));
        Armor armor2 = new Armor("Bizarre Cloth", 1, ArmorType.LEATHER,
                Slot.BODY, new HeroAttribute(2,4,1));
        ranger.equipArmor(armor);
        ranger.equipArmor(armor2);
        List<Float> expected = new ArrayList<>();
        expected.add(3.0f);
        expected.add(11.0f);
        expected.add(2.0f);
        List<Float> actual = ranger.totalAttributes();

        assertEquals(expected, actual);
    }
    @Test
    public void damage_noEquipment_shouldReturnDamageOne() {
        Hero ranger = new Ranger("Goran");
        double expected = 1;
        double actual = ranger.damage();

        assertEquals(expected, actual);
    }
    @Test
    public void damage_with_Weapon_shouldReturnTotalDamage() throws InvalidWeaponException {
        Hero ranger = new Ranger("Goran");
        Weapon weapon = new Weapon("Matthew Longstaff", 1,
                WeaponType.BOW, 1);
        ranger.equipWeapon(weapon);
        float expected = 1f * (1f + 7f/100f);
        float actual =  ranger.damage();

        assertEquals(expected, actual);
    }
    @Test
    public void damage_replaceWeapon_shouldReturnTotalDamage() throws InvalidWeaponException {
        Hero ranger = new Ranger("Goran");
        Weapon weapon = new Weapon("Matthew Longstaff", 1,
                WeaponType.BOW, 1);
        Weapon weapon2 = new Weapon("Sean Longstaff", 1,
                WeaponType.BOW, 3);
        ranger.equipWeapon(weapon);
        ranger.equipWeapon(weapon2);

        float expected = 3F*(1F+ 7/100F);
        float actual = ranger.damage();

        assertEquals(expected, actual);
    }
    @Test
    public void damage_equipWeaponAndArmor_shouldReturnTotalDamage() throws InvalidWeaponException, InvalidArmorException {
        Hero ranger = new Ranger("Goran");
        Weapon weapon = new Weapon("Matthew Longstaff", 1,
                WeaponType.BOW, 1);
        Armor armor = new Armor("Magic Cloth", 1, ArmorType.LEATHER,
                Slot.BODY, new HeroAttribute(1,2,3));
        ranger.equipWeapon(weapon);
        ranger.equipArmor(armor);

        float expected = 1F*(1F+ 9F/100F);
        float actual = ranger.damage();

        assertEquals(expected, actual);
    }
    @Test
    public void ranger_display_shouldReturnHerosState() {
        Hero ranger = new Ranger("Goran");

        String expected = "Name = " + ranger.getName() + "\n" +
                "Class = " + ranger.getClass().getSimpleName() + "\n" +
                "Level = " + ranger.getLevel() + "\n" +
                "Total Strength = "+ ranger.totalAttributes().get(0) + "\n" +
                "Total Dexterity = " + ranger.totalAttributes().get(1) + "\n" +
                "Total Intelligence = " + ranger.totalAttributes().get(2) + "\n" +
                "Total Damage = " + ranger.damage();

        String actual = ranger.display();

        assertEquals(expected, actual);
    }

}
