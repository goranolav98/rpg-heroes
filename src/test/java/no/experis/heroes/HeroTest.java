package no.experis.heroes;

import static org.junit.jupiter.api.Assertions.*;

import no.experis.heroes.types.Mage;
import org.junit.jupiter.api.Test;

class HeroTest {
    @Test
    public void hero_correctName_shouldReturnName() {
        Hero mage = new Mage("Goran");
        String expectedName = "Goran";
        String actualName = mage.getName();

        assertEquals(expectedName, actualName);
    }


    @Test
    public void hero_correctLevel_shouldReturnLevel() {
        Hero mage = new Mage("Goran");
        int expectedLevel = 1;
        int actualLevel = mage.getLevel();

        assertEquals(expectedLevel, actualLevel);
    }

    @Test
    public void hero_attributes_dexterity_shouldReturnDexterity(){
        Hero mage = new Mage("Goran");
        HeroAttribute hero = new HeroAttribute(1,1,8);
        double expected = hero.getDexterity();

        HeroAttribute heroMage = mage.getLevelAttributes();
        double actual = heroMage.getDexterity();

        assertEquals(expected,actual);
    }
    @Test
    public void hero_attributes_strength_shouldReturnStrength(){
        Hero mage = new Mage("Goran");
        HeroAttribute hero = new HeroAttribute(1,1,8);
        double expected = hero.getStrength();

        HeroAttribute heroMage = mage.getLevelAttributes();
        double actual = heroMage.getStrength();

        assertEquals(expected,actual);
    }
    @Test
    public void hero_attributes_intelligence_shouldReturnIntelligence(){
        Hero mage = new Mage("Goran");
        HeroAttribute hero = new HeroAttribute(1,1,8);
        double expected = hero.getIntelligence();

        HeroAttribute heroMage = mage.getLevelAttributes();
        double actual = heroMage.getIntelligence();

        assertEquals(expected,actual);
    }

}
