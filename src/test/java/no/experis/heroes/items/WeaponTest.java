package no.experis.heroes.items;

import no.experis.heroes.items.itemType.WeaponType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WeaponTest {
    @Test
    public void weapon_name_shouldReturnCorrectName() {
        Weapon weapon = new Weapon("Matthew Longstaff", 1,
                WeaponType.STAFF, 2);
        String expected = "Matthew Longstaff";
        String actual = weapon.getItemName();

        assertEquals(expected, actual);
    }
    @Test
    public void weapon_lvl_shouldReturnCorrectLvl(){
        Weapon weapon = new Weapon("Matthew Longstaff", 1,
                WeaponType.STAFF, 2);
        int expected = 1;
        int actual= weapon.getRequiredLevel();
        assertEquals(expected,actual);
    }
    @Test
    public void weapon_weaponType_shouldReturnCorrectType() {
        Weapon weapon = new Weapon("Matthew Longstaff", 1,
                WeaponType.STAFF, 2);
        WeaponType expected = WeaponType.STAFF;
        WeaponType actual = weapon.getWeaponType();

        assertEquals(expected, actual);
    }
    @Test
    public void weapon_weaponSlot_shouldReturnWeapon() {
        Slot expected = Slot.WEAPON;
        Weapon weapon = new Weapon("Matthew Longstaff", 1,
                WeaponType.STAFF, 2);
        Slot actual = weapon.getSlot();

        assertEquals(expected, actual);
    }
    @Test
    public void weapon_damage_shouldReturnCorrectDamage() {
        Weapon weapon = new Weapon("Matthew Longstaff", 1,
                WeaponType.STAFF, 2);
        int expected = 2;
        int actual = weapon.getWeaponDamage();

        assertEquals(expected, actual);
    }

}
