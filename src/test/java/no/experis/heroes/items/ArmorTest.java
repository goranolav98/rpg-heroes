package no.experis.heroes.items;

import static org.junit.jupiter.api.Assertions.*;

import no.experis.heroes.HeroAttribute;
import no.experis.heroes.items.itemType.ArmorType;
import org.junit.jupiter.api.Test;


class ArmorTest {
    @Test
    public void armor_name_shouldReturnCorrectName() {
        Armor armor = new Armor("Magic Cloth", 1, ArmorType.CLOTH,
                Slot.BODY, new HeroAttribute(1,2,3));
        String expected = "Magic Cloth";
        String actual = armor.getItemName();

        assertEquals(expected, actual);
    }
    @Test
    public void armor_lvl_shouldReturnCorrectLvl(){
        Armor armor = new Armor("Magic Cloth", 1, ArmorType.CLOTH,
                Slot.BODY, new HeroAttribute(1,2,3));
        int expected = 1;
        int actual= armor.getRequiredLevel();
        assertEquals(expected,actual);
    }
    @Test
    public void armor_armorType_shouldReturnCorrectType() {
        Armor armor = new Armor("Magic Cloth", 1, ArmorType.CLOTH,
                Slot.BODY, new HeroAttribute(1,2,3));
        ArmorType expected = ArmorType.CLOTH;
        ArmorType actual = armor.getArmorType();

        assertEquals(expected, actual);
    }
    @Test
    public void armor_armorSlot_shouldReturnCorrectSlot() {
        Armor armor = new Armor("Magic Cloth", 1, ArmorType.CLOTH,
                Slot.BODY, new HeroAttribute(1,2,3));
        Slot expected = Slot.BODY;
        Slot actual = armor.getSlot();

        assertEquals(expected, actual);
    }
    @Test
    public void armor_ArmorStrength_shouldReturnArmorStrength(){
        Armor armor = new Armor("Magic Cloth", 1, ArmorType.CLOTH,
                Slot.BODY, new HeroAttribute(1,2,3));
        double expected = 1;
        double actual = armor.getArmorAttribute().getStrength();

        assertEquals(expected, actual);

    }
    @Test
    public void armor_ArmorDexterity_shouldReturnArmorDexterity(){
        Armor armor = new Armor("Magic Cloth", 1, ArmorType.CLOTH,
                Slot.BODY, new HeroAttribute(1,2,3));
        double expected = 2;
        double actual = armor.getArmorAttribute().getDexterity();

        assertEquals(expected, actual);

    }
    @Test
    public void armor_ArmorIntelligence_shouldReturnArmorIntelligence(){
        Armor armor = new Armor("Magic Cloth", 1, ArmorType.CLOTH,
                Slot.BODY, new HeroAttribute(1,2,3));
        double expected = 3;
        double actual = armor.getArmorAttribute().getIntelligence();

        assertEquals(expected, actual);

    }





}
